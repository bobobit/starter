<?php

class Frontend_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

		// Login check
		$exception_uris = array(
				'user/login',
				'user/logout'
		);

		$this->load->library('ion_auth');

		if (in_array(uri_string(), $exception_uris) == FALSE) {
			if ($this->ion_auth->logged_in() == FALSE) {
				redirect('user/login');
			} elseif ($this->ion_auth->logged_in() == TRUE && $this->is_forbidden() == TRUE) {
				redirect('user/login');
			}
		}

    }

    private function is_forbidden(){

		$check_group='forbidden'; $id=false; $check_all = true;

		return (bool) $this->ion_auth->in_group($check_group, $id, $check_all);

    }
}
