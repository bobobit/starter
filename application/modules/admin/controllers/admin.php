<?php
/**
 * Admin Controller
 */
class Admin extends Admin_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->library('grocery_CRUD');

	}
	
	public function index()
    {
		$output = new stdClass();
		$this->template->set_layout('_admin_layout');
		$this->template->build('index', $output);
    }

	public function users()
	{
		$output = new stdClass();
		$this->template->set_layout('_admin_layout');	
		$this->template->build('users', $output);
	}	

	public function iframe_users()
	{
		try{
			
			$crud = new grocery_CRUD();
						
			$crud->set_theme('flexigrid');
			$crud->set_subject('user');
			$crud->set_table('users');
			$crud->field_type('password', 'password');
			$crud->required_fields('type','username','password');
			$crud->set_rules('username', 'Username', 'required|min_length[5]|max_length[12]|is_unique[users.username]');
			$crud->set_rules('password', 'Password', 'required|min_length[5]|max_length[50]');
			$crud->callback_before_insert(array($this,'encrypt_password_callback'));
			
			$output = $crud->render();

			$this->load->view('subview', $output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function _password_column_width($value,$row) {
	    return "<span style=\"width:50%; max-width:100px; display:block;\">".$value."</span>";
	}


	public function groups()
	{
		$output = new stdClass();
		$this->template->set_layout('_admin_layout');	        
		$this->template->build('groups', $output);
	}
	
	public function iframe_groups()
	{
		try{

			$crud = new grocery_CRUD();
			
			$crud->set_theme('flexigrid');
			$crud->set_table('groups');
			$crud->set_subject('Groups');
/*			$crud->display_as('name','');
			$crud->field_type('name', 'readonly');
			$crud->display_as('value','');
			$crud->unset_add();
			$crud->unset_delete();*/
			
			$output = $crud->render();

			$this->load->view('subview', $output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function users_groups()
	{
		$output = new stdClass();
		$this->template->set_layout('_admin_layout');	        
		$this->template->build('users_groups', $output);
	}

	public function iframe_users_groups()
	{
		try{

			$crud = new grocery_CRUD();
			
			$crud->set_theme('flexigrid');
			$crud->set_table('users_groups');
			$crud->set_subject('Users groups');
/*			$crud->field_type('name', 'readonly');
			$crud->display_as('value','');
			$crud->unset_add();
			$crud->unset_delete();*/
			
			$output = $crud->render();

			$this->load->view('subview', $output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function login_attempts()
	{
		$output = new stdClass();
		$this->template->set_layout('_admin_layout');	        
		$this->template->build('login_attempts', $output);
	}

	public function iframe_login_attempts()
	{
		try{

			$crud = new grocery_CRUD();
			
			$crud->set_theme('flexigrid');
			$crud->set_table('login_attempts');
			$crud->set_subject('Login attempts');
			$crud->field_type('name', 'readonly');
			$crud->display_as('value','');
			$crud->unset_add();
			$crud->unset_delete();
			
			$output = $crud->render();

			$this->load->view('subview', $output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function ionauth_users()
	{
		$output = new stdClass();

		$this->load->module('user');
        $output->subview = $this->user->users(true);

		$this->template->set_layout('_admin_layout');	
		$this->template->build('ionauth_subview', $output);

		
	}	

	public function create_user()
	{
		$output = new stdClass();

		$this->load->module('user');
        $output->subview = $this->user->create_user(true);

		$this->template->set_layout('_admin_layout');	
		$this->template->build('ionauth_subview', $output);

	}
	
	public function edit_user($id)
	{
		$output = new stdClass();

		$this->load->module('user');
        $output->subview = $this->user->edit_user($id, true);

		$this->template->set_layout('_admin_layout');	
		$this->template->build('ionauth_subview', $output);

	}

	public function create_group()
	{
		$output = new stdClass();

		$this->load->module('user');
        $output->subview = $this->user->create_group(true);

		$this->template->set_layout('_admin_layout');	
		$this->template->build('ionauth_subview', $output);

	}
	
	public function edit_group($id)
	{
		$output = new stdClass();

		$this->load->module('user');
        $output->subview = $this->user->edit_group($id, true);

		$this->template->set_layout('_admin_layout');	
		$this->template->build('ionauth_subview', $output);

	}

	public function activate($id, $code=false)
	{
		$output = new stdClass();

		$this->load->module('user');
        $output->subview = $this->user->activate($id);

		$this->template->set_layout('_admin_layout');	
		$this->template->build('ionauth_subview', $output);

	}

	public function deactivate($id = NULL)
	{
		$output = new stdClass();

		$this->load->module('user');
        $output->subview = $this->user->deactivate($id, true);

		$this->template->set_layout('_admin_layout');	
		$this->template->build('ionauth_subview', $output);

	}

}