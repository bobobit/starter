<div class="modal-header">
	<h4 class="modal-title">Log in</h4>
	<p>Admin - Please log in using your credentials</p>
</div>
<div class="modal-body">

	<?php echo validation_errors(); ?>
	<?php echo form_open();?>
	<?php echo form_hidden('login_to', 'proceed_to_checkout');?>
	<table class="table">
		<tr>
			<td>Email</td>
			<td><?php echo form_input('email'); ?></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><?php echo form_password('password'); ?></td>
		</tr>
		<tr>
			<td></td>
			<td><?php echo form_submit('submit', 'Admin Log in', 'class="btn btn-primary"'); ?></td>
		</tr>
	</table>
	<?php echo form_close();?>
</div>

<div class="modal-footer">
	        
