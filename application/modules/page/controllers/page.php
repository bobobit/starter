<?php
/**
 * Page Controller
 */
class Page extends Frontend_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->template->set_layout('_both_sides_sidebar_layout');
	}
	
	public function index()
    {

        $data['page_heading'] = 'Naslov na stranata';
        $data['page_data'] = 'Sodrzina na stranata vo skratena forma.';
        $data['page_details'] = 'Prodolzenie na prethodniot tekst.';
        
		$this->template->build('page', $data);
    }

}