<?php $this->load->view('partials/_header');?>	

<div class="row">

	<div class="col-lg-3">
		<?php $this->load->view('partials/left_sidebar');?>
	</div>

	<div class="col-lg-9">
    	<?=($template['body'] ? $template['body'] : '')?>
    </div>

</div><!-- /row -->  

<?php $this->load->view('partials/_footer');?>