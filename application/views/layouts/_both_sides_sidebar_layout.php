<?php $this->load->view('partials/_header');?>	

<div class="row">

	<div class="col-lg-2">
		<?php $this->load->view('partials/left_sidebar');?>
	</div>

	<div class="col-lg-8">
    	<?=($template['body'] ? $template['body'] : '')?>
    </div>

	<div class="col-lg-2">
		<?php $this->load->view('partials/right_sidebar');?>
	</div>

</div><!-- /row -->  

<?php $this->load->view('partials/_footer');?>